# shoppinPalTest

This project is a test given by shoppinpal.com . This test includes following tasks to complete<br />

1. Create an application by using loopback.io as the backend server.

2. The Loopback API server is built on top of the Express web server which is well known in the Node.js community.

3. You may use any frontend framework like angular js and any CSS framework like bootstrap or materializecss to make your work easy.

4. Your application should have an attractive signup and login page. This is where your UI/UX skillset also shines, so be simple and creative. After login, the username should show on the page somewhere indicating who has logged in.

5. Your application should work with the memory connector in loopback. You must configure it to save to file. You don't need a database.

6. A boot script should automatically create three users when the server starts.

7. Your application should allow the logged-in user to create a category. Editing and deleting a category should also be possible. This is again your chance to showcase your UI/UX skillset. Keep it simple, functional and beautiful.

8. User should be able to drill down into a view for a selected category and add a parent category or a child category or both. Users should not add any new categories, they should use those that have already been created. User should be able to search for existing categories by providing a typeahead match on leading characters. Your application can still continue to display all the categories in the main view as a flat list.

Following dependencies must be met and install globaly before running the project

1. npm and node
2. bower
3. strongloop

To run this project simply clone it from git

`git clone https://Dextor4494@bitbucket.org/Dextor4494/shoppinpaltest.git`

And then run npm command to install dependencies locally it will install bower files as well

`npm install`

Now run project by command

`slc run`
