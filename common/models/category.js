'use strict';

module.exports = function(Category) {

  //// href: http://stackoverflow.com/questions/15523514/find-by-key-deep-in-a-nested-object
  //function getObject(theObject, instance) {
  //  var result = null;
  //  if(theObject instanceof Array) {
  //    for(var i = 0; i < theObject.length; i++) {
  //      result = getObject(theObject[i], instance);
  //      if (result) {
  //        break;
  //      }
  //    }
  //  }
  //  else
  //  {
  //    for(var prop in theObject) {
  //      console.log(prop + ': ' + theObject[prop]);
  //      if(prop == 'id') {
  //        if(theObject[prop] == instance.id) {
  //          return theObject;
  //        }
  //      }
  //      if(theObject[prop] instanceof Object || theObject[prop] instanceof Array) {
  //        result = getObject(theObject[prop], instance);
  //        if (result) {
  //          break;
  //        }
  //      }
  //    }
  //  }
  //  return result;
  //}
  ///*
  //* function to update all categories data for single category change
  //* */
  //function editCategoryTree(instance){
  //  Category.find({}, function(err, categories) {
  //    var category = getObject(categories, instance);
  //    category.title = instance;
  //  });
  //}

  Category.observe('after save', function(ctx, next){
    // check if data field is not null if yes then add current category
    // as parent category
    if(ctx.instance && ctx.instance.data.length == 0){
      // initialize default data list
      ctx.instance.data.push({
        id: ctx.instance.id,
        title: ctx.instance.name,
        nodes: []
      });
      // save the instance
      ctx.instance.save();
    }
    // continue execution
    next();
  })
};
