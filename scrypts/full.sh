#!/usr/bin/env bash
#!author Aadil

# I am skipping server basic setup part, dev user creation and env setup
# Lets jump directly to project setup part
# asuming dev ops user as devops1, dev-loopback(non-sudoer) and group for it is devops
# no need to create users in localhost

sudo apt-get update && sudo apt-get upgrade
# href: http://unix.stackexchange.com/questions/281774/ubuntu-server-16-04-cannot-get-supervisor-to-start-automatically
echo 'Install Supervisor'
sudo apt-fast -y install supervisor
# in 16.04 run this to start supervisor on reboot
# Make sure Supervisor comes up after a reboot.
sudo systemctl enable supervisor

# Bring Supervisor up right now.
sudo systemctl start supervisor
# install npm first
sudo apt-get install npm
# install node
sudo apt-get install nodejs-legacy
# first install strongloop for loopback framework
# you will need sudo permission for it as we are installing it globally
sudo npm install -g strongloop
# install bower
sudo npm install bower -g
# now pull project from the bitbucket
# if you are authorized to server then u will be able to pull it successfully
cd ~ && git clone git@bitbucket.org:Dextor4494/shoppinpaltest.git
# now move project to another safe dir as you want to get you server hacked in matter of minutes
#####Create Users and developer group(--system is to create a system account)
sudo groupadd --system webapps
#add user 'dev-django' with its home directory as /opt/tmq_django
sudo useradd --system --gid webapps --shell /bin/bash --home /home/dev-node1 dev-node1
# [Skip this part for localhost]
sudo mv shoppinpaltest /opt/
# Change ownership
sudo chown  -R dev-node1:webapps /opt/shoppinpaltest
# change permission of project for our env
#Reset the default permissions
# so that devops Group can work in there
# [Skip this part for localhost]
sudo chgrp -R devops /opt/shoppinpaltest
# [Skip this part for localhost]
#dev-loopback is the owner and files belong to the devops group
sudo chown -R dev-loopback:devops /opt/shoppinpaltest

################CREATE SYMLINKS##############################
echo "adding symlink for process manager"
sudo ln -s /opt/shoppinpaltest/server/config/supervisor/logic_test.conf /etc/supervisor/conf.d/logic_test.conf
sudo ln -s /opt/shoppinpaltest/server/config/nginx/logic_test /etc/nginx/sites-available/logic_test
sudo ln -s /opt/shoppinpaltest/server/config/nginx/logic_test /etc/nginx/sites-enabled/logic_test

sudo supervisorctl reread
sudo supervisorctl update
# [Skip this part for localhost]
#so that developers can read/write/execute, others can read(2775 is for others can )
sudo chmod -R 2775 /opt/shoppinpaltest
sudo chmod -R 775 /opt/shoppinpaltest

# install project dependencies
cd /opt/shoppinpaltest
npm install

# test server using strong loop command
slc run
