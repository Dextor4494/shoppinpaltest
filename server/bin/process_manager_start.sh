#!/usr/bin/env bash

echo "**************Process manager Starting **************************"
echo "Starting $NAME as `whoami`"


NODEP_DIR=/opt/shoppinpaltest       # node project directory

cd $NODEP_DIR


# Start your nodejs process manager
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec slc run
