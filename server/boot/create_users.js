/**
 * Created by aadil on 1/11/16.
 */
/**
 * This file create 3 users on each boot
 * As it is inside server/boot so it will get executed every time server boot
 * */
// ref: https://docs.strongloop.com/display/public/LB/Registering+users
// ref: https://docs.strongloop.com/display/public/LB/Creating+a+default+admin+user

module.exports = function(app){
  app.models.User.create([
      {username: 'arya', email: 'arya@gmail.com', password: '12345678'},
      {username: 'John snow', email: 'john.snow@gmail.com', password: '12345678'},
      {username: 'Sansa', email: 'sansa@gmail.com', password: '12345678'}
    ], function(err, users){
      if(err) return console.log('%j', err);
      // now create admin role in project
      app.models.Role.create({
        name: 'admin'
      }, function(err, role) {
        if(err) return console.log(err);
        // print in console about role
        console.log(role);
        // making John snow admin and leader of night watch
        // while sansa and arya are building themselves in other states
        role.principals.create({
          principalType: app.models.RoleMapping.USER,
          principalId: users[1].id
        }, function(err, pricipals) {
          // printing more debug inside consoles
          if (err) return console.log(err);
          console.log(pricipals);
        })
      })
    }
  );
};
