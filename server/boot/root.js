'use strict';

var path = require("path");

module.exports = function(server) {
  // Install a `/` route that returns server status
  var router = server.loopback.Router();
  router.get('/', function(req, res){res.sendFile(path.resolve(__dirname + '../../../static/views/base.html'))});
  server.use(router);
};
