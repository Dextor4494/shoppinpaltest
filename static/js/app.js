/**
 * Created by aadil on 2/11/16.
 */
(function() {
  'use strict';
  angular.module('shoppinPalTest', [
    'lbServices',
    'ui.router',
    'ui.tree',
    'toastr',
    'ui.tree-filter'
  ])
  .config(routeConfig)
  .run(function($state) {
    $state.go($state.get('login'), {}, {});
  });

  // a function that handle all routes
  // since it is small project so I am handlig routes here
  function routeConfig($stateProvider, uiTreeFilterSettingsProvider, toastrConfig) {
    // configuring notification settings
    angular.extend(toastrConfig, {
      closeButton: true,
      closeHtml: '<button>&times;</button>',
      timeOut: 5000,
      autoDismiss: false,
      containerId: 'toast-container',
      maxOpened: 0,
      newestOnTop: true,
      positionClass: 'toast-top-right',
      preventDuplicates: false,
      preventOpenDuplicates: false,
      target: 'body',
      template: null
    });
    // configuring tree search for names
    uiTreeFilterSettingsProvider.addresses = ['title', 'description', 'username'];
    // configuring routes in app
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: '/views/login.html',
        controller: 'loginCtrl',
        controllerAs: 'login'
      })
      .state('signup', {
        url: '/signup',
        templateUrl: '/views/signup.html',
        controller: 'signUpCtrl',
        controllerAs: 'signup'
      })
      .state('category', {
        url: '/category',
        templateUrl: '/views/category.html',
        controller: 'categoryViewCtrl',
        controllerAs: 'category'
      })
  }
})();
