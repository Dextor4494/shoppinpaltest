(function() {
	'use strict';
	angular
	  .module('shoppinPalTest')
	  .controller('loginCtrl', loginCtrl)
	  .controller('signUpCtrl', signUpCtrl)
	  .controller('categoryViewCtrl', categoryViewCtrl)
	  .controller('categoryListCtrl', categoryListCtrl)
	  .controller('categoryTreeViewCtrl', categoryTreeViewCtrl);

	loginCtrl.$inject = ['$state', 'User', 'toastr'];
	function loginCtrl($state, User, toastr) {
		var login = this;
		// check if user is logged in or not
		if(User.isAuthenticated())
			$state.go($state.get('category'), {}, {});
		// redirect to signup page on click
		login.redirectSignUp = function(){
			$state.go($state.get('signup', {}, {reload: true}));
		}
		// loggin in user
		login.loginUser = function(){
			User.login({
				email: login.email,
				password: login.password
			}, function(data) {
				$state.go($state.get('category'), {}, {});
			}, function(err) {
				toastr.error(err.data.error.message, 'Error');
			});
		}
	}
	
	signUpCtrl.$inject = ['$state', 'User', 'toastr'];
	function signUpCtrl($state, User, toastr) {
		var signup = this;
		// check if user is logged in or not
		if(User.isAuthenticated())
			$state.go($state.get('category'), {}, {});
		
		// to signup a new user
		signup.signUpUser = function() {
			// validate data first
			if(!signup.username)
				toastr.error("Please enter username.");
			else if(!signup.email)
				toastr.error("Please enter email address.");
			else if(signup.password.length < 8)
				toastr.error("Password should contains at least 8 characters.");
			else{
				// now create a user and redirect him to login page
				User.create({
					username: signup.username,
					email: signup.email,
					password: signup.password
				}, function(user) {
					$state.go($state.get('login'), {}, {});
				}, function(err) {
					toastr.error(err.data.error.message, 'Error');;
				});
			}
		};
	}
	
	categoryViewCtrl.$inject = ['$state', '$timeout', 'Category', 'categoryService', 'User', 'toastr'];
	function categoryViewCtrl($state, $timeout, Category, categoryService, User, toastr) {
		var category = this;
		category.modal = {};
		// check if user is logged in or not
		if(!User.isAuthenticated())
			$state.go($state.get('login'), {}, {});
		else
			User.getCurrent(function(user) {
				category.user = user;
			});
		
		// find all categories 
		category.category_list = Category.find();
		categoryService.setCategoryList(category.category_list);

		// function to log out logged in user
		category.logoutUser = function(){
			// loggin out user
			User.logout(
				// success cb
				function(data){
					$state.go($state.get('login'), {}, {});
				},
				// error cb
				function(err){
					toastr.error(err.data.error.message, 'Error');;
				}
			);
		}
	}

	categoryListCtrl.$inject = ['$scope', '$state', '$timeout', 'Category', 'categoryService', 'toastr'];
	function categoryListCtrl($scope, $state, $timeout, Category, categoryService, toastr) {
		var vm = this;
		vm.modal = {};
		// assigning category list from parent controller
		vm.category_list = $scope.category.category_list;

		vm.confirmDelete = function(category){
			vm.select_category = category;
			$("#confirmDelete").modal('show');
		}

		// function to edit new/existing category
		vm.editCategory = function(type, category){
			if(category)
				vm.modal = category;
			vm.modal.title = type + ' category';
			$('#categoryModal').modal('show');
		}

		// function to save Category
		vm.saveCategory = function(category){
			// removing unwanted attr
			// parent node have name not title
			delete category['title'];
			Category.replaceOrCreate(category);
			$('#categoryModal').modal('hide');
			// using timeout bcoz modal closing effect takes 1sec
			$timeout(function(){
				$state.go($state.current, {}, {reload: true});
			}, 1000)
		};

		// function to delete a main category
		vm.deleteCategory = function(){
			Category.deleteById({
				id: vm.select_category.id
			}, function() {
				toastr.success("Category deleted successfully.")
			}, function (err) {
				toastr.error(err.data.error.message, 'Error');;
			});
			$("#confirmDelete").modal('hide');
			// using timeout bcoz modal closing effect takes 1sec
			$timeout(function() {
				$state.go($state.current, {}, {reload: true});
			}, 1000);
		}

		// function to select category on click
		vm.selectCategory = function(category) {
			// unselect all category
			vm.category_list.filter(function(item) {
				if(item.meta)
					item.meta.isSelected = false;
				return item;
			});
			// adding meta attr to track if it is selected or not
			category.meta = {};
			category.meta.isSelected = true;
			// set new data to service
			categoryService.setCategory(category);
			categoryService.broadCast();
		};
	}

	categoryTreeViewCtrl.$inject = ['$scope', 'Category', 'categoryService', 'toastr'];
	function categoryTreeViewCtrl($scope, Category, categoryService, toastr) {
		var vm = this;
		vm.select_category = {};
		// bind data in controller from service
		vm.category = categoryService.getCategory();
		vm.category_list = categoryService.getCategoryList();

		vm.remove = function (scope) {
			if(vm.category.title)
				delete vm.category['title'];
			scope.remove();
			// update category
			Category.replaceOrCreate(
			vm.category, 
			function(data) {

			}
			, function(err) {
				toastr.error(err.data.error.message, 'Error');;
			});
		};

		vm.toggle = function (item) {
			item.collapsed = !item.collapsed;
		};

		// function to add new node in tree
		vm.newSubItem = function (scope) {
			// if parent node have unwanted attr then remove it 
			if(vm.category.title)
				delete vm.category['title'];
			// if select_category if null then show alert
			if(!vm.select_category.hasOwnProperty('id'))
				toastr.error("Please select a category.")
			else{
				// get nodes data
				var nodeData = scope.$modelValue;
				// push new node data into it 
				nodeData.nodes.push({
					id: vm.select_category.id,
					title: vm.select_category.name,
					nodes: []
				});

				// update category
				Category.replaceOrCreate(
				vm.category, 
				function(data) {

				}
				, function(err) {
					toastr.error(err.data.error.message, 'Error');;
				});
			}
			// hide the modal after operation
			$("#categorySelectModal").modal('hide');
		};

		// function modal to select/add category in tree
		vm.openSelectCategoryModal = function(scope){
			vm.scope = scope;
			$("#categorySelectModal").modal('show');
		}

		// catch the events for category updated
		$scope.$on(categoryService.broadCastStr, function() {
			vm.category = categoryService.getCategory();
			vm.category_list = categoryService.getCategoryList();
		});
	}
})();