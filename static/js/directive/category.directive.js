(function() {
    'use strict';

    angular
        .module('shoppinPalTest')
        .directive('categoryList', categoryList)
        .directive('categoryTreeView', categoryTreeView)

    function categoryList() {
        var directive = {
            templateUrl: '/views/directive/category_list_view.html',
            controller: 'categoryListCtrl',
            controllerAs: 'cl',
            restrict: 'E'            
        };
        return directive;
    }

    function categoryTreeView() {
        var directive = {            
            templateUrl: '/views/directive/category_tree_view.html',
            controller: 'categoryTreeViewCtrl',
            controllerAs: 'ca',
            restrict: 'E'            
        };
        return directive;
    }

})();