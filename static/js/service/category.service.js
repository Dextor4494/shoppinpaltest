(function() {
'use strict';

    angular
        .module('shoppinPalTest')
        .service('categoryService', categoryService);
    
    categoryService.$inject = ['$rootScope']
    function categoryService($rootScope) {
        this._category = {};
        this._category_list = [];
        // broadCastStr by which event can be catched
        this.broadCastStr = 'categorySelect';

        // setter function for category
        this.setCategory = function(category) {
            this._category = category;
        }
        
        this.setCategoryList = function(category_list) {
            this._category_list = category_list;
        }

        this.broadCast = function() {
            $rootScope.$broadcast(this.broadCastStr);
        }

        // getter function for category
        this.getCategory = function() {
            return this._category;
        }
        // getter function for category
        this.getCategoryList = function() {
            return this._category_list;
        }
    }
})();